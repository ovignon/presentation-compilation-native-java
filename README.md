# Insallation de l'environnement de démo

## Connexion à la machine de démo
- `ssh jnatif@vm-am` (pwd "jnatif"), où "vm-am" est un alias de la VM atway-machine 10.21.236.85

## Installation de programmes tiers sur (root) :
- CentOS :
  - `yum install gcc glibc-devel zlib-devel libstdc++-static`
- Debian :
  - `sudo apt-get install libc6-dev zlib1g-dev`

## Installation de GraalVM et de Maven (avec le user "jnatif" dans le répertoire /home/jnatif) :
- Téléchargement d'une version spécifique de GraalVM : `wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-22.2.0/graalvm-ce-java17-linux-amd64-22.2.0.tar.gz`
- Téléchargement de la dernière version de GraalVM : `bash <(curl -sL https://get.graalvm.org/jdk)`
- Extraction de GraalVM : `tar xzvf graalvm-ce-java17-linux-amd64-22.2.0.tar.gz`
- Téléchargement de Maven : `wget https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.tar.gz`
- Extraction de Maven : `tar xzvf apache-maven-3.8.6-bin.tar.gz`
- Ajouter dans le fichier /home/jnatif/.bashrc :
```
export JAVA_HOME=/home/jnatif/graalvm-ce-java17-22.2.0
export PATH=$JAVA_HOME/bin:/home/jnatif/apache-maven-3.8.6/bin:$PATH
```
- Se déconnecter et se reconnecter
- Vérifier la version de java : `java -version` -> doit afficher "openjdk version "17.0.4" 2022-07-19" et "OpenJDK Runtime Environment GraalVM CE 22.2.0"
- Intaller l'extension "native image" de GraalVM : `gu install native-image`


Installation des sources (avec le user "jnatif" dans le répertoire /home/jnatif) :
- `git clone https://gitlab.com/ovignon/presentation-compilation-native-java.git`
