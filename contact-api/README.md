Code généré à partir de https://code.quarkus.io/
## Installation de Postgresql
- Créer un alias "postgres-contact" avec l'IP du serveur Postgresql dans /etc/hosts
- Exécuter le compose-file.yml présent dans le répertoire docker/posgres : `docker-compose up -d`
- Exemple de connection avec psql : `psql -Umobility -hlocalhost -p5454 -dmobility`
- Init des données : `psql -Umobility -hlocalhost -dmobility -p5454 < sql/init-db.sql`
## Build
- Jar : `./mvnw clean package -DskipTests` (9s)
- Natif : `./mvnw clean package -DskipTests -Dnative` (environ 4min30s)
## Run
- JVM : `java -jar target/quarkus-app/quarkus-run.jar` (1.658s)
- Natif : `./target/contact-api-1.0.0-SNAPSHOT-runner` (0.7s, environ 24 fois plus vite)
- Consommation mémoire :
  - JVM : 140 Mo
  - Natif : 32 Mo (environ 4.3 fois moins)
- Benchmark HTTP : `httping localhost:8080/contact`
  - JVM :
  ```
  connected to 127.0.0.1:8080 (69 bytes), seq=0 time=367.59 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=1 time= 12.33 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=2 time=  6.46 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=3 time= 11.50 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=4 time= 12.26 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=5 time= 10.68 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=6 time=  8.26 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=7 time=  5.59 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=8 time=  9.87 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=9 time= 10.20 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=10 time= 10.79 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=11 time=  7.89 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=12 time=  6.81 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=13 time=  9.31 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=14 time= 12.11 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=15 time= 10.67 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=16 time=  9.55 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=17 time=  7.28 ms
  ```
  ==> consommation CPU entre 1 et 2%
  - Natif :
  ```
    connected to 127.0.0.1:8080 (69 bytes), seq=0 time= 27.10 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=1 time=  2.29 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=2 time=  4.41 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=3 time=  2.85 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=4 time=  3.47 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=5 time=  3.01 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=6 time=  3.21 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=7 time=  2.17 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=8 time=  2.99 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=9 time=  3.12 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=10 time=  3.42 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=11 time=  3.07 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=12 time=  2.84 ms
  connected to 127.0.0.1:8080 (69 bytes), seq=13 time=  2.87 ms
  ```
   ==> consommation CPU négligeable (0%)
## Conteneurisation
- Création de l'image Docker à partir de l'éxécutable natif (avec une image de base "micro") : `docker build -f src/main/docker/Dockerfile.native-micro -t jnatif/contact-api-micro .`
- Exécution de la stack dans le répertoire "docker/stack" : `docker-compose up -d`
