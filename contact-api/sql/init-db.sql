drop table if exists contact;
create table contact(id uuid primary key not null, first_name varchar(255), last_name varchar(255), email varchar(255));

insert into contact (id, first_name, last_name, email) values (gen_random_uuid(), 'Stan', 'March', 'stan.march@google.com');
insert into contact (id, first_name, last_name, email) values (gen_random_uuid(), 'Kyle', 'Broflovski', 'kyle.broflovski@google.com');
insert into contact (id, first_name, last_name, email) values (gen_random_uuid(), 'Kenny', 'McCormick', 'kenny.mccormick@google.com');
insert into contact (id, first_name, last_name, email) values (gen_random_uuid(), 'Eric', 'Cartman', 'eric.cartman@google.com');

select * from contact;

