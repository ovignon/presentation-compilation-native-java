-- This file allow to write SQL commands that will be emitted in test and dev.

insert into contact (id, first_name, last_name, email) values (gen_random_uuid(), 'Stan', 'March', 'stan.march@google.com');
insert into contact (id, first_name, last_name, email) values (gen_random_uuid(), 'Eric', 'Cartman', 'eric.cartman@google.com');
