package com.mobility.jnatif.api.contact.converters;

import com.mobility.jnatif.api.contact.dataobjects.Contact;
import com.mobility.jnatif.api.contact.dto.ContactDto;
import com.mobility.jnatif.api.contact.entities.ContactEntity;

public class ContactConverter {

	private ContactConverter() {
	}

	public static Contact entityToDataObject(ContactEntity entity) {
		if (entity == null) {
			return null;
		}
		return new Contact(entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getEmail());
	}

	public static ContactDto dataObjectToDto(Contact dataObject) {
		if (dataObject == null) {
			return null;
		}
		return new ContactDto(dataObject.getId(), dataObject.getFirstName(), dataObject.getLastName(),
				dataObject.getEmail());
	}

	public static Contact dtoToDataObject(ContactDto dto) {
		if (dto == null) {
			return null;
		}
		return new Contact(dto.getId(), dto.getFirstName(), dto.getLastName(), dto.getEmail());
	}

	public static ContactEntity dataObjectToEntity(Contact dataObject) {
		if (dataObject == null) {
			return null;
		}
		return new ContactEntity(dataObject.getId(), dataObject.getFirstName(), dataObject.getLastName(),
				dataObject.getEmail());
	}

}
