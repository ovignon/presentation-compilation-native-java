package com.mobility.jnatif.api.contact.dataobjects;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Contact {
	UUID id;
	String firstName;
	String lastName;
	String email;
}
