package com.mobility.jnatif.api.contact.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "contact")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactEntity {
	@Id
	UUID id;
	@Column(name = "first_name")
	String firstName;
	@Column(name = "last_name")
	String lastName;
	String email;
}
