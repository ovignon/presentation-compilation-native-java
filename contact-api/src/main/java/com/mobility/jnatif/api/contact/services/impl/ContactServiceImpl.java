package com.mobility.jnatif.api.contact.services.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.google.common.collect.Lists;
import com.mobility.jnatif.api.contact.converters.ContactConverter;
import com.mobility.jnatif.api.contact.dataobjects.Contact;
import com.mobility.jnatif.api.contact.entities.ContactEntity;
import com.mobility.jnatif.api.contact.repositories.ContactRepository;
import com.mobility.jnatif.api.contact.services.ContactService;

import org.jboss.logging.Logger;

@ApplicationScoped
public class ContactServiceImpl implements ContactService {

	private final Logger logger = Logger.getLogger(ContactServiceImpl.class.getName());

	@Inject
	ContactRepository contactRepository;

	@Override
	@Transactional
	public Contact findById(UUID id) {
		logger.infov("Appel de ContactServiceImpl.findById({0})", id);
		Optional<ContactEntity> optionalEntity = contactRepository.findById(id);
		if (optionalEntity.isPresent()) {
			return ContactConverter.entityToDataObject(optionalEntity.get());
		} else {
			logger.warnv("Entité non trouvée : {}", id);
			return null;
		}
	}

	@Override
	@Transactional
	public List<Contact> findAll() {
		logger.infov("Appel de ContactServiceImpl.findAll()");
		List<ContactEntity> entities = Lists.newArrayList(contactRepository.findAll());
		return entities.stream().map(entity -> ContactConverter.entityToDataObject(entity))
				.collect(Collectors.toList());
	}

	@Override
	@Transactional
	public Contact save(Contact dataObject) {
		logger.infov("Appel de ContactServiceImpl.save({0})", dataObject);
		if (dataObject.getId() == null) {
			dataObject.setId(UUID.randomUUID());
		}
		ContactEntity entity = ContactConverter.dataObjectToEntity(dataObject);
		entity = contactRepository.save(entity);
		return ContactConverter.entityToDataObject(entity);
	}

	@Override
	@Transactional
	public void delete(Contact dataObject) {
		logger.infov("Appel de ContactServiceImpl.delete({0})", dataObject);
		ContactEntity entity = ContactConverter.dataObjectToEntity(dataObject);
		contactRepository.delete(entity);
	}

	@Override
	@Transactional
	public void deleteById(UUID id) {
		logger.infov("Appel de ContactServiceImpl.deleteById({0})", id);
		contactRepository.deleteById(id);
	}

}
