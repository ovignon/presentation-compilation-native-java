package com.mobility.jnatif.api.contact.api;

import java.util.List;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mobility.jnatif.api.contact.dto.ContactDto;

@Path("/contact")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface ContactApi {

	@GET
	@Path("/{id}")
	public ContactDto findById(UUID id);

	@GET
	@Path("/")
	public List<ContactDto> findAll();

	@POST
	@Path("/")
	public ContactDto create(ContactDto dto);

	@PUT
	@Path("/")
	public ContactDto update(ContactDto dto);

	@DELETE
	@Path("/{id}")
	public void deleteById(UUID id);

}
