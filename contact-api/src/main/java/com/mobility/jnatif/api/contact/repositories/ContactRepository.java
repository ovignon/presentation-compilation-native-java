package com.mobility.jnatif.api.contact.repositories;

import java.util.UUID;

import com.mobility.jnatif.api.contact.entities.ContactEntity;

import org.springframework.data.repository.CrudRepository;

public interface ContactRepository extends CrudRepository<ContactEntity, UUID> {
}
