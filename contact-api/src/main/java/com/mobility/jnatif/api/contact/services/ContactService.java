package com.mobility.jnatif.api.contact.services;

import java.util.List;
import java.util.UUID;

import com.mobility.jnatif.api.contact.dataobjects.Contact;

public interface ContactService {

	Contact findById(UUID id);

	List<Contact> findAll();

	Contact save(Contact dataObject);

	void delete(Contact dataObject);

	void deleteById(UUID id);

}
