package com.mobility.jnatif.api.contact.dto;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactDto {
	UUID id;
	String firstName;
	String lastName;
	String email;
}
