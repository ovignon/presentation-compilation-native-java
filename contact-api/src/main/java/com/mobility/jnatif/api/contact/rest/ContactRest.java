package com.mobility.jnatif.api.contact.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.mobility.jnatif.api.contact.api.ContactApi;
import com.mobility.jnatif.api.contact.converters.ContactConverter;
import com.mobility.jnatif.api.contact.dataobjects.Contact;
import com.mobility.jnatif.api.contact.dto.ContactDto;
import com.mobility.jnatif.api.contact.services.ContactService;

import org.springframework.util.CollectionUtils;

import io.vertx.ext.web.handler.HttpException;

public class ContactRest implements ContactApi {

	@Inject
	ContactService service;

	@Override
	public ContactDto findById(UUID id) {
		if (id == null) {
			throw new HttpException(500);
		}
		Contact dataObject = service.findById(id);
		if (dataObject != null) {
			return ContactConverter.dataObjectToDto(dataObject);
		} else {
			return null;
		}
	}

	@Override
	public List<ContactDto> findAll() {
		List<Contact> dataObjectList = service.findAll();
		if (CollectionUtils.isEmpty(dataObjectList)) {
			return new ArrayList<>();
		} else {
			return dataObjectList.stream().map(dataObject -> ContactConverter.dataObjectToDto(dataObject))
					.collect(Collectors.toList());
		}
	}

	@Override
	public ContactDto create(ContactDto dto) {
		if (dto == null) {
			throw new HttpException(500);
		}
		Contact dataObject = ContactConverter.dtoToDataObject(dto);
		dataObject = service.save(dataObject);
		return ContactConverter.dataObjectToDto(dataObject);
	}

	@Override
	public ContactDto update(ContactDto dto) {
		if (dto == null || dto.getId() == null || findById(dto.getId()) == null) {
			throw new HttpException(500);
		}
		Contact dataObject = ContactConverter.dtoToDataObject(dto);
		dataObject = service.save(dataObject);
		return ContactConverter.dataObjectToDto(dataObject);
	}

	@Override
	public void deleteById(UUID id) {
		if (id == null || findById(id) == null) {
			throw new HttpException(500);
		}
		service.deleteById(id);
	}

}
